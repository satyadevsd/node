const express = require("express");
const app = express();
app.use(express.urlencoded());
app.use("/public",express.static(__dirname+"/public"));
app.get("/",function(req,res){res.sendFile(__dirname + "/public/signup.html");});
app.listen(process.env.PORT||3000);
app.post("/",function(req,res){
  const fname = req.body.fname;
  const lname = req.body.lname;
  const email = req.body.email;
  const client = require("@mailchimp/mailchimp_marketing");
  client.setConfig({
    apiKey: "44572538bf7e545ef74425fcd93d0db3-us21",
    server: "us21",
  });
  const run = async () => {
    const response = await client.lists.batchListMembers("62e850ee04", {members: [
          {
            email_address:email,
            status:"subscribed",
            merge_fields:{FNAME:fname,LNAME:lname}
          }],});
        if(response.total_created=="1")
        {
          res.sendFile(__dirname + '/public/success.html')
        }
        else
        {
          res.sendFile(__dirname + '/public/failure.html')
        }
    };
  run();
});
app.post("/public/failure", function (req, res) {
res.sendFile(__dirname + '/public/signup.html');
});
